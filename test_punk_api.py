import requests
import datetime


def test_default_beers():
    response = requests.get("https://api.punkapi.com/v2/beers")
    body = response.json()
    assert body[0]["id"] == 1
    assert body[-1]["id"] == 25
    assert len(body) == 25


def test_ids_11_to_20():
    params = {
        "ids": "11|12|13|14|15|16|17|18|19|20"
    }
    response = requests.get("https://api.punkapi.com/v2/beers", params=params)
    body = response.json()
    assert len(body) == 10

    beer_id = 11
    for beer in body:
        assert beer["id"] == beer_id
        beer_id += 1


def test_beer_id_123():
    response = requests.get("https://api.punkapi.com/v2/beers?ids=123")
    body = response.json()
    assert len(body) == 1
    assert body[0]["id"] == 123
    assert body[-1]["id"] == 123
    for beer in body:
        assert beer["name"] == "Candy Kaiser"


def test_20_beers_from_page_5():
    response = requests.get("https://api.punkapi.com/v2/beers?page=5&per_page=20")
    body = response.json()
    assert len(body) == 20
    assert body[0]["id"] == 81
    assert body[-1]["id"] == 100

    beer_id = 81
    for beer in body:
        assert beer["id"] == beer_id
        beer_id += 1


def test_beers_with_abv_from_5_to_7():
    response = requests.get("https://api.punkapi.com/v2/beers?abv_gt=4.9&abv_lt=7.1")
    body = response.json()
    for beer in body:
        assert beer["abv"] >= 5
        assert beer["abv"] <= 7


def test_beer_first_brewed_in_year_2010():
    response = requests.get("https://api.punkapi.com/v2/beers?brewed_before=01-2011&brewed_after=12-2009")
    body = response.json()
    first_brewed = (
                "01/2010" or "02/2010" or "03/2010" or "04/2010" or "05/2010" or "06/2010" or "07/2010" or "08/2010" or "09/2010" or "10/2010" or "11/2010" or "12/2010")

    for beer in body:
        assert beer["first_brewed"] == first_brewed
