from faker import Faker
from utilsSRC.gorest_handler import GoRESTHandler

gorest_handler = GoRESTHandler()


def test_create_user():
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    body = gorest_handler.get_user_by_id(user_id).json()
    assert body["email"] == user_data["email"]
    assert body["name"] == user_data["name"]

def test_update_user():
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    user_update_data = {
        "name": Faker().name(),
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    assert user_update_data

    response = gorest_handler.update_user(user_update_data)
    assert response.status_code == 200

    body = gorest_handler.update_user(user_data, user_update_data).json()
    assert "id" in body

    user_id = body["id"]
    assert user_id

    response = gorest_handler.update_user(user_id).json()
    assert response.status_code == 200
    assert response.content
    assert body["email"] == user_update_data["email"]
    assert body["name"] == user_update_data["name"]

def test_delete_user():
    user_data = {
        "name": "Tenali Ramakrishna",
        "gender": "male",
        "email": Faker().email(),
        "status": "active"
    }
    body = gorest_handler.create_user(user_data).json()
    assert "id" in body
    user_id = body["id"]
    gorest_handler.delete_user(user_id)
    response = gorest_handler.get_user_by_id(user_id)
    assert response.status_code == 404
