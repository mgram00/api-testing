import requests


def test_json_is_not_empty():
    """Pokemon API: assert /v2/pokemon is not empty"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()
    assert len(body) != 0


def test_if_response_code_is_200():
    """Pokemon API: assert /v2/pokemon response has status 200"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()
    assert response.status_code == 200


def test_pokemon_number_is_1279():
    """Pokemon API: assert /v2/pokemon response has 1279 items"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()
    assert body["count"] == 1279


def test_response_time_is_under_1s():
    """Pokemon API: assert /v2/pokemon response is under 1 second"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()
    #response_time_ms = response.elapsed.microsekonds // 1000
    # assert response_time_ms < 1000
    assert response.elapsed.total_seconds() < 1


def test_response_size_is_under_100kb():
    """Pokemon API assert /v2/pokemon response is under 100 KB"""
    response = requests.get("https://pokeapi.co/api/v2/pokemon")
    body = response.json()
    response_size_kb = len(response.content) / 1000
    assert response_size_kb < 100


def test_pagination():
    """Pokemon API assert /v2/pokemon pagination works properly"""
    params = {
        "limit": 10,
        "offset": 20
    }
    response = requests.get("https://pokeapi.co/api/v2/pokemon", params=params)
    body = response.json()
    assert len(body["results"]) == params["limit"]
    assert body["results"][0]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset']+1}/"
    assert body["results"][-1]["url"] == f"https://pokeapi.co/api/v2/pokemon/{params['offset']+params['limit']}/"
    assert len(body["results"]) == 10
    splitted = (body["results"][0]['url']).split("/")
    assert int(splitted[-2]) == (params["offset"] + 1)

