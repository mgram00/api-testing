import requests


class GoRESTHandler:
    base_url = "https://gorest.co.in/public/v2"
    user_endpoint = "/users"

    headers = {
        "Authorization": "Bearer 8b8dc21c7c35b935fc2a12cd41a236336fe7ad13b6fde24f4b087fd34da0d7a3"
    }

    def create_user(self, user_data, expected_status_code=201):
        response = requests.post(self.base_url + self.user_endpoint, json=user_data, headers=self.headers)
        assert response.status_code == expected_status_code
        return response

    def get_user_by_id(self, user_id):
        response = requests.get(f"{self.base_url}{self.user_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 200
        return response

    def update_user(self, user_id, user_data):
        response = requests.put(f"{self.base_url}{self.user_endpoint}/{user_id}", json=user_data, headers=self.headers)
        assert response.status_code == 200
        return response

    def delete_user(self, user_id):
        response = requests.delete(f"{self.base_url}{self.user_endpoint}/{user_id}", headers=self.headers)
        assert response.status_code == 200
        return response
